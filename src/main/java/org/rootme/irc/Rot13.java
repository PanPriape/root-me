/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.rootme.irc;

import java.io.IOException;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author panpriape
 */
public class Rot13 extends PircBot {
    
    private static char[] alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};

    /**
     * @return the alphabet
     */
    public static char[] getAlphabet() {
        return alphabet;
    }
    
    public Rot13(){
    	this.setName("Malade");
    	this.setLogin("Malade");
    }

    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message){
        if(sender.equals("Candy")){
                System.out.println(message);
              	sendMessage("Candy", "!ep3 -rep " + this.rot13(message));
        }
    }
    
    public String rot13(String message) {
        
        String rep = "";
        
        for (int i = 0; i < message.length(); i++) {
            char inter = message.charAt(i);
            
            if ( (int)inter == 32 ) {
                rep += inter;
            } else if ( (int)inter >= 48 && (int)inter <= 57 ) {
                rep += inter;
            } else  if ( (int)inter >= 65 && (int)inter <= 90) {
                rep += rot13(inter,true);
            } else {
               rep += rot13(inter,false); 
            }
                    
        }
        
        return rep;
    }
    
    private char rot13(char chara, boolean mode) {
        char rep = chara;
        int index = 0;
        
        if (mode) {
            index = this.findChar( (char) ( (int) rep + 32));
            index = (index + 13) % Rot13.getAlphabet().length;
            
            return (char) ( (int)Rot13.getAlphabet()[index] -32);

        } else {
            index = this.findChar(rep);
            index = (index + 13) % Rot13.getAlphabet().length;

            return Rot13.getAlphabet()[index];

        }
    }
    
    public int findChar(char chara) {
        int count = 0;
        
        while (count < getAlphabet().length && chara != getAlphabet()[count]) {
            count ++;
        }
        
        return count;
    }
    
    public void resolve() throws IOException, IrcException {
        this.setAutoNickChange(true);
        this.setVerbose(true);
        this.connect("irc.root-me.org");
        this.dccSendChatRequest("Candy", 5);
        this.sendMessage("Candy", "!ep3");
        //this.disconnect();
    }
    
    
}
