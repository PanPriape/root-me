/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.rootme.irc;

import java.io.IOException;
import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.PircBot;

/**
 *
 * @author panpriape
 */
public class GoBackToCollege extends PircBot {
    
    public GoBackToCollege(){
    	this.setName("Test");
    	this.setLogin("Test");
    }

    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message){
        if(sender.equals("Candy")){
            if (message.contains("/")) {
                int separateur = message.indexOf("/");
                double nb1 = Integer.parseInt(message.substring(0, separateur - 1));
                double nb2 = Integer.parseInt(message.substring(separateur + 2, message.length()));
                //System.out.println(nb1 + " " + nb2);
                double resultat = Math.sqrt(nb1) * nb2;
                resultat = Math.round(resultat * Math.pow(10,2)) / Math.pow(10,2);
                sendMessage("Candy", "!ep1 -rep " + resultat);
            } else {
                System.out.println(message);
            }
              	
        }
    }
    
    public void resolve() throws IOException, IrcException {
        this.setAutoNickChange(true);
        this.setVerbose(true);
        this.connect("irc.root-me.org");
        this.dccSendChatRequest("Candy", 5);
        this.sendMessage("Candy", "!ep1");
        //this.disconnect();
    }
}
